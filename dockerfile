FROM node:10 AS builder
WORKDIR /usr/src/server.js
COPY ./package*.json ./
RUN yarn add server.js
COPY . .

FROM node:10-alpine
WORKDIR /usr/src/server.js
COPY --from=builder /usr/src/server.js ./
EXPOSE 3333
CMD ["yarn", "dev"]