module.exports = {
    dialect: 'postgres',
    host: 'ec2-52-86-207-245.compute-1.amazonaws.com',
    username: 'postgres',
    password: '123',
    database: 'sqlnode',
    define: {
      timestamps: true,
      underscored: true,
    },
  };