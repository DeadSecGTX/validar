'use strict';

const faker = require('faker');

module.exports = {
  up: (queryInterface, Sequelize) => {
    
    let data = [];
    let registros = 5;

    while(registros--){
      let date = new Date();
        data.push({
            descricao: faker.internet.userName(),
            created_at: date,
            updated_at: date
        });
    }
    return queryInterface.bulkInsert('nivel', data, {});

  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('nivel', null, {});
  }
};
