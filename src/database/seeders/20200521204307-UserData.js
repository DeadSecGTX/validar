'use strict';

const faker = require('faker');

module.exports = {
  up: (queryInterface, Sequelize) => {

    let data = [];
    let registros = 5;

    while(registros--){
      let date = new Date();
        data.push({
            nome: faker.internet.userName(),
            email: faker.internet.email(),
            crm: faker.internet.password(),
            cre: faker.internet.password(),
            nivel_id: 1,
            status: false,
            created_at: date,
            updated_at: date
        });
    }
    return queryInterface.bulkInsert('user', data, {});
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('user', null, {});
  }
};
