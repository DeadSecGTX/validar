const User = require('../models/Users');
const { Op } = require("sequelize");

module.exports = {

    async index(req, res)
    {
            try{

                const users = await User.findAll({
                    where: { status: true}
                });
                return res.status(200).json(users);
            }
            catch(err)
            {
                return res.status(400).send({error : err});
            }
    },

    async listar(req, res)
    {

        const { nome } = req.params;
        
        const usuario = await User.findAll({
            where: {
                nome: {
                  [Op.like]: `%${nome}%`
                },
                status : true
              }
        });
        if(!usuario){
            return res.status(400).json({ error: 'Usuario nao encotrado'});
        }
        else{
            return res.status(200).json(usuario);
        }
        

    },

    async unico(req, res)
    {
        const { id } =  req.body;

        const usuario = await User.findByPk(id)

        if(!usuario)
        {
            return res.status(400).json({ error: 'Usuario nao encotrado'});
        }
            return res.status(200).json(usuario);
    },

    async store(req, res)
    {
      
        const { nome, email, crm, cre, nivel, status} = req.body;

        if (cre == null || cre == "")
        {
            //é um médico
            if(await User.findOne({
                where: {
                  [Op.or]: [
                    { email: email },
                    { crm: crm },
                  ],
                }
            }))
            {
                return res.status(400).json({ error: 'Médico ja cadastrado'});
            }

            const user = await User.create({ nome, email, crm, cre : null, nivel, status});

            return res.status(200).json(user);    

        }

        //É enfermeiro
        if(await User.findOne({
            where: {
              [Op.or]: [
                { email: email },
                { cre: cre },
              ],
            }
        }))
        {
            return res.status(400).json({ error: 'Enfermeiro ja cadastrado'});
        }
    
        const user = await User.create({ nome, email, crm : null, cre, nivel, status});

        return res.status(200).json(user);    
       
    },

    async alterar(req, res)
    {
        const { usr_id } =  req.params;
        const { nome, email, crm, cre } = req.body
        
        const usuario = await User.findByPk(usr_id);

        if(!usuario)
        {
            return res.status(400).json({ error: 'Usuario nao encotrado'});
        }

        const a = await User.findOne({
            where: {
              [Op.or]: [
                { email: email },
                { crm: crm },
                { cre: cre}
              ]
            }
        });

        if(a == null)
        {

        usuario.nome = nome;
        usuario.email = email;
        usuario.crm = crm;
        usuario.cre = cre;
        
        await usuario.save();

        return res.status(200).json(usuario);

        }

        if(a.id != usuario.id)
        {
            return res.status(400).json({ error: 'Informações ja cadastradas'});
        }

        usuario.nome = nome;
        usuario.email = email;
        usuario.crm = crm;
        usuario.cre = cre;
        
        await usuario.save();

        return res.status(200).json(usuario);

    },

    async excluirprovisorio(req, res)
    {

        const { usr_id } =  req.params;

        const usuario = await User.findByPk(usr_id)

        if(!usuario)
        {
            return res.status(400).json({ error: 'Usuario nao encotrado'});
        }
     
        await User.destroy({
            where: {id : usr_id}
        });

        return res.status(200).json({ msg: 'Usuario excluido com sucesso'});

    },

    async inativar(req, res)
    {

        const { usr_id } =  req.params;
        const usuario = await User.findByPk(usr_id)

        if(!usuario)
        {
            return res.status(400).json({ error: 'Usuario nao encotrado'});
        }
        
        usuario.status = false;
        
        await usuario.save();

        return res.status(200).json(usuario);


    }


    
}