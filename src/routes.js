const express = require('express');
const UserController = require('./controllers/UserController');
const LoginController =  require('./controllers/LoginController')
const NivelController = require('./controllers/NivelController');
const routes = express.Router();

routes.post('/users', UserController.store);
routes.post('/logar/:usr_id/ativar', LoginController.ativar);
routes.get('/index', UserController.index);
routes.post('/index', UserController.unico);
routes.get('/listar/:nome', UserController.listar);
routes.post('/excluir/:usr_id', UserController.excluirprovisorio);
routes.post('/alterar/:usr_id', UserController.alterar);
routes.post('/inativar/:usr_id', UserController.inativar);
routes.post('/users/:usr_id/login', LoginController.store);
routes.post('/logar', LoginController.logar);
routes.post('/nivel', NivelController.store);
routes.get('/nivel/index', NivelController.index);

module.exports = routes;
